#pragma once
#include "Account.h"
#include "ATM.h"
#include <memory>

class FundsManager
{
public:
    FundsManager(std::shared_ptr<Account> account, std::shared_ptr<ATM> atm);

    ~FundsManager();

    void GetCurrentBalance(int code) const;

    bool Withdraw(int code, int cashToWithdraw);

    void Deposit(int code, int cashToDeposit);

private:
    std::shared_ptr<Account> m_Account;
    std::shared_ptr<ATM> m_ATM;
};