/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
#pragma once

class ATM
{
public:
    ATM();
    ~ATM();

    void AddCash(int amount);
    void WithdrawCash(int amount);
    int CheckRemainingCash();

private:
    int m_Amount;
};