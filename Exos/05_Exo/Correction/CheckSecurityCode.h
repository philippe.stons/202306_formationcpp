#pragma once

class CheckSecurityCode
{
public:
    CheckSecurityCode();
    ~CheckSecurityCode();

    void SetAccountNbr(int accountNbr);

    bool CheckCode(int code);

private:
    int m_ActiveAccount;
};