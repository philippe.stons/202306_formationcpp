/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */

#include <iostream>
#include <functional>
#include <exception>
#include <thread>
#include <atomic>
#include <future>
#include<unistd.h>
#include <mutex>
#include <syncstream>

class Response
{
public:
    std::string value;
};

class ConnectionException: public std::exception
{
public:
    const char* what() const throw()
    {
        return "Connection error !";
    }
};

template<typename T>
class AbstractConnection
{
public:
    AbstractConnection() {
        std::cout << "BASE CONSTRUCT" << std::endl;
        this->status = false;
    }

    virtual ~AbstractConnection()
    {
        std::cout << "BASE DESTROY" << std::endl;
        if(this->connection)
        {
            this->connection->join();
            dynamic_cast<T*>(this)->StopConnection();
            delete this->connection;
        }
    }

    virtual void StartConnection(std::function<void(Response&)> res) = 0;
    virtual void StartConnection(std::function<void(Response&)> res, std::function<void()> err) = 0;
    virtual void StopConnection() = 0;

    const bool GetStatus() const {
        return this->status;
    }

protected:
    std::thread* connection; 
    std::function<void(Response&)> res;
    std::function<void()> err;
    std::atomic<bool> status;
};

class TcpConnection : public AbstractConnection<TcpConnection>
{
public:
    TcpConnection(int port)
        : port(port)
    {
        
    }

    ~TcpConnection()
    {
        
    }

    virtual void StartConnection(std::function<void(Response&)> res) final {
        this->StartConnection(res, nullptr);
    }

    virtual void StartConnection(std::function<void(Response&)> res, 
        std::function<void()> err) final {
        this->res = res;
        this->err = err;
        this->status = true;
        
        this->connection = new std::thread([&] {
            try
            {
                std::cout << "Start Connection " << std::endl;
                sleep(5);
                Response resp;
                resp.value = " - hello world - ";
                this->res(resp);
                std::cout << "test after sub" << std::endl;
                this->status = false;
            }
            catch(const ConnectionException& e)
            {
                std::cerr << e.what() << '\n';
                if(this->err)
                {
                    this->err();
                }
                this->status = false;
            }
        });
    }

    virtual void StopConnection() final {
        std::cout << "Stop Connection" << std::endl;
    }

private:
    int port;
};

int main()
{
    TcpConnection connection(5432);
    connection.StartConnection([](Response& resp) {
        std::cout << "SUBSCIPTION" << resp.value << std::endl;
    });

    while(connection.GetStatus()) {
        sleep(1);
        std::cout << "WAITING" << std::endl;
    }
}