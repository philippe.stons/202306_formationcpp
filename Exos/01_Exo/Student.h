/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/20/23.
//

#ifndef FORMATION_C_CPP_STUDENT_H
#define FORMATION_C_CPP_STUDENT_H

#include <string>
#include <vector>

class Student {
public:
    Student() = delete;
    Student(std::string firstName, std::string lastName, int age);

    ~Student();

    void learn();
private:
    std::string m_firstName;
    std::string m_lastName;
    int m_age;

};


#endif //FORMATION_C_CPP_STUDENT_H
