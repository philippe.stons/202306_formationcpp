/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/20/23.
//

#include "Teacher.h"
#include <iostream>

// Teacher::Teacher() {

// }

Teacher::Teacher(std::string firstName, std::string lastName, int age)
    : m_firstName(firstName), m_lastName(lastName), m_age(age)
{
    m_subjects = std::vector<std::string>();
}

Teacher::~Teacher()
{

}

void Teacher::teach()
{
    int subject = rand() % m_subjects.size();
    std::cout << "Today we will learn about " << m_subjects[subject] << std::endl;
}

void Teacher::AddSubject(std::string subject)
{
    m_subjects.push_back(subject);
}
