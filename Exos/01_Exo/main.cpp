/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/20/23.
//
#include "Teacher.h"
#include "Student.h"
#include "ClassRoom.h"

int main()
{
    Teacher teacher("John", "Doe", 42);
    teacher.AddSubject("Maths");
    teacher.AddSubject("Physics");

    Student student1("Jane", "Doe", 18);
    Student student2("Jack", "Doe", 19);
    Student student3("Jill", "Doe", 20);

    ClassRoom classRoom(3, teacher);
    // classRoom.SetTeacher(teacher);
    classRoom.AddStudent(student1);
    classRoom.AddStudent(student2);
    classRoom.AddStudent(student3);

    classRoom.StartClass();
    return 0;
}