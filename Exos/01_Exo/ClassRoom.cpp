/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/20/23.
//

#include "ClassRoom.h"
#include <iostream>

ClassRoom::ClassRoom(int maxStd, Teacher teacher)
    : m_MaxStd(maxStd), m_Teacher(teacher)
{
    m_Students = std::vector<Student>();
    m_Students.reserve(m_MaxStd);
}

ClassRoom::~ClassRoom()
{

}

void ClassRoom::AddStudent(Student student)
{
    if(m_MaxStd >= m_Students.size()) {
        m_Students.push_back(student);
    }
}

void ClassRoom::SetTeacher(Teacher teacher)
{
    m_Teacher = teacher;
}

void ClassRoom::StartClass() {
    m_Teacher.teach();

    for (auto student : m_Students) {
        student.learn();
    }
}
