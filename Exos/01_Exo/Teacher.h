/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/20/23.
//

#ifndef FORMATION_C_CPP_TEACHER_H
#define FORMATION_C_CPP_TEACHER_H

#include <string>
#include <vector>

class Teacher {
public:
    Teacher() = delete;
    Teacher(std::string firstName, std::string lastName, int age);

    ~Teacher();

    void teach();

    void AddSubject(std::string subject);

private:
    std::string m_firstName;
    std::string m_lastName;
    int m_age;
    std::vector<std::string> m_subjects;
};


#endif //FORMATION_C_CPP_TEACHER_H
