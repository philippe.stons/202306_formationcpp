/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/20/23.
//

#include "Student.h"
#include <iostream>

Student::Student(std::string firstName, std::string lastName, int age)
    : m_firstName(firstName), m_lastName(lastName), m_age(age)
{

}

Student::~Student()
{

}

void Student::learn()
{
    int skip = rand() % 10;

    if (skip == 0) {
        std::cout << "I'm skipping class today" << std::endl;
    } else {
        std::cout << "I'm learning" << std::endl;
    }
}
