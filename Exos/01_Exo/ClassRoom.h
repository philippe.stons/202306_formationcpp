/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/20/23.
//

#ifndef FORMATION_C_CPP_CLASSROOM_H
#define FORMATION_C_CPP_CLASSROOM_H

#include <vector>
#include "Teacher.h"
#include "Student.h"


class ClassRoom {
public:
    ClassRoom() = delete;
    ClassRoom(int maxStd, Teacher teacher);

    ~ClassRoom();

    void AddStudent(Student student);
    void SetTeacher(Teacher teacher);

    void StartClass();

private:
    int m_MaxStd;
    Teacher m_Teacher;
    std::vector<Student> m_Students;
};


#endif //FORMATION_C_CPP_CLASSROOM_H
