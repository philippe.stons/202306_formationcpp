/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
//
// Created by rmdir on 6/21/23.
//

#ifndef FORMATION_C_CPP_EXO03_H
#define FORMATION_C_CPP_EXO03_H

#include <string>

class Animal {
public:
    Animal() = delete;
    Animal(std::string Name,
           int Weight,
           int Height,
           bool Sex,
           int Age,
           float DeathProbability);
    virtual ~Animal();

    virtual void Crier() = 0;

protected:
    std::string m_Name;
    int m_Weight;
    int m_Height;
    bool m_Sex;
    int m_Age;
    float m_DeathProbability;
};

class Dog : public Animal {
public:
    Dog() = delete;
    Dog(std::string Name,
        int Weight,
        int Height,
        bool Sex,
        int Age,
        bool Trained,
        std::string Race);
    ~Dog();

    void Crier();

private:
    bool m_Trained;
    std::string m_Race;
};

enum class CatTemper {
    CALM = 5,
    AGGRESSIVE = 12,
    PLAYFUL = 32
};

enum class DogTemper {
    CALM,
    AGGRESSIVE,
    PLAYFUL
};

class Cat : public Animal {
public:
    Cat() = delete;
    Cat(std::string Name,
        int Weight,
        int Height,
        bool Sex,
        int Age,
        bool LongHair,
        bool ClawsCut,
        std::string Temper);
    ~Cat();

    void Crier();

private:
    bool m_LongHair;
    bool m_ClawsCut;
    std::string m_Temper;
};

class Bird : public Animal {
public:
    Bird() = delete;
    Bird(std::string Name,
         int Weight,
         int Height,
         bool Sex,
         int Age,
         bool RequireAviary,
         std::string Color);
    ~Bird();

    void Crier();

private:
    bool m_RequireAviary;
    std::string m_Color;
};

#endif //FORMATION_C_CPP_EXO03_H
