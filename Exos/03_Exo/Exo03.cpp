#include "Exo03.h"
#include <iostream>

Animal::Animal(std::string Name, int Weight, int Height, bool Sex, int Age, float DeathProbability)
    : m_Name(Name), m_Weight(Weight), m_Height(Height), m_Sex(Sex), m_Age(Age), m_DeathProbability(DeathProbability)
{
}

Animal::~Animal()
{

}

Dog::Dog(std::string Name, int Weight, int Height, bool Sex, int Age, bool Trained, std::string Race)
    : Animal(Name, Weight, Height, Sex, Age, 1.0f), m_Trained(Trained), m_Race(Race)
{
}

Dog::~Dog()
{

}

void Dog::Crier()
{
    std::cout << "Wouf" << std::endl;
}

Cat::Cat(std::string Name, int Weight, int Height, bool Sex, int Age, bool LongHair, bool ClawsCut, std::string Temper)
    : Animal(Name, Weight, Height, Sex, Age, 0.5f), m_LongHair(LongHair), m_ClawsCut(ClawsCut), m_Temper(Temper)
{

}

Cat::~Cat()
{

}

void Cat::Crier()
{
    std::cout << "Miaou" << std::endl;
}

Bird::Bird(std::string Name, int Weight, int Height, bool Sex, int Age, bool RequireAviary, std::string Color)
    : Animal(Name, Weight, Height, Sex, Age, 3.0f), m_RequireAviary(RequireAviary), m_Color(Color)
{

}

Bird::~Bird()
{

}

void Bird::Crier()
{
    std::cout << "Cui cui" << std::endl;
}

