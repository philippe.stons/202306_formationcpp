#pragma once
#include <string>

namespace UnitTest
{
    class TestClass
    {
    public:
        TestClass();
        TestClass(int value);
        ~TestClass();

        void SetValue(int value);

        const int GetValue() const
        {
            return m_Value;
        }

        const std::string GetStringValue() const
        {
            return std::to_string(m_Value);
        }


    private:
        int m_Value;
    };
}