#include "UnitTestClass.h"

namespace UnitTest
{
    TestClass::TestClass()
    {
        m_Value = 50;
    }

    TestClass::TestClass(int value)
    {
        m_Value = value;
    }

    TestClass::~TestClass()
    {

    }

    void TestClass::SetValue(int value)
    {
        m_Value = value;
    }

}