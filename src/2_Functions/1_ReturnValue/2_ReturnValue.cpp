#include "ReturnValue.h"

#include <iostream>

void ReturnNothing();

int ReturnAnInt();

void* ReturnAVoidPtr();

int* ReturnAnIntPtr();

int* AttentionToScope();

std::shared_ptr<int> ReturnASharedIntPtr();

int& ReturnAnIntRef();

int& WarningIntRef();

void ReturnExample()
{
    int Integer = ReturnAnInt();
    int* FromVoidPtr = (int*) ReturnAVoidPtr();
    int* FromtIntPtr = ReturnAnIntPtr();

    std::cout << Integer << std::endl;
    std::cout << *FromVoidPtr << std::endl;
    std::cout << *FromtIntPtr << std::endl;
    
    delete FromVoidPtr;
    delete FromtIntPtr;

    //int* ScopeWarning = AttentionToScope();
    //std::cout << *ScopeWarning << std::endl;
    
    std::shared_ptr<int> SharedPtr = ReturnASharedIntPtr();
    std::cout << *SharedPtr << std::endl;

    int Ref = ReturnAnIntRef();
    //Will segfault
    //Ref = WarningIntRef();
    Ref = 5;
}

void ReturnNothing()
{

}

int ReturnAnInt()
{
    return 999;
}

void* ReturnAVoidPtr()
{
    int* ptr = new int(85);
    return ptr;
}

int* ReturnAnIntPtr()
{
    int* ptr = new int(951);
    return ptr;
}

int* AttentionToScope()
{
    int ret = 51;
    return &ret;
}

std::shared_ptr<int> ReturnASharedIntPtr()
{
    std::shared_ptr<int> ptr = std::make_shared<int>(51);
    return ptr;
}

int& ReturnAnIntRef()
{
    static int i = 10;
    return i;
}

int& WarningIntRef()
{
    int ret = 10;

    /*
    ret is a variable linked to this scope.
    So ret will be destroyed once the function is done.
    It'll then try to return it but will fail.
    */
    return ret;
}