#include "2_Reference.h"
#include "Debuging/ObjectWithCopyMove/ObjCpyMvCnstr.h"
#include <iostream>

void CopyComp(TestObject NonRef);
void RefComp(TestObject& Ref);
void PtrComp(TestObject* Ref);
void ConstRef(const TestObject& Ref);

void ReferenceExample()
{

    TestObject Obj;
    TestObject::ResetObjectStat();

    std::cout << "\n No reference: " << std::endl;
    CopyComp(Obj);
    std::cout << "Value is: " << Obj.val << " and should be 51." << std::endl;
    Obj.PrintObjectStat();
    Obj.ResetObjectStat();

    std::cout << "\n Reference: " << std::endl;
    RefComp(Obj);
    std::cout << "Value is: " << Obj.val << " and should be 95." << std::endl;
    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();

    std::cout << "\n Pointer: " << std::endl;
    PtrComp(&Obj);
    std::cout << "Value is: " << Obj.val << " and should be 105." << std::endl;
    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();
}

void CopyComp(TestObject NonRef)
{
    NonRef.val = 51;
}

void RefComp(TestObject& Ref)
{
    Ref.val = 95;
}

void PtrComp(TestObject* Ref)
{
    Ref->val = 105;
}

void ConstRef(const TestObject& Ref)
{
    
}