#include "MultipleReturnVal.h"
#include <string>
#include <tuple>

struct RetVal
{
    int IntRet;
    std::string StringRet;
};

void ReferenceToStruct(RetVal& Ret);
RetVal ReturnStruct();
std::tuple<int, std::string>ReturnTwoTuple();

void MultipleReturnValueExample()
{
    RetVal Ret;
    ReferenceToStruct(Ret);
    Ret = ReturnStruct();

    //Cpp 11
    std::tuple<int, std::string> retTuple = ReturnTwoTuple();
    Ret.IntRet = std::get<0>(retTuple);
    Ret.StringRet = std::get<1>(retTuple);
    
    //Cpp 17
    auto[IntVal, StrVal] = ReturnTwoTuple();

    Ret.IntRet = IntVal;
    Ret.StringRet = StrVal;
}

RetVal ReturnStruct()
{
    //this will also work.
    //RetVal Ret = { 51 , "Return this string" };
    //return Ret;
    return { 51, "Return this string" };
}

void ReferenceToStruct(RetVal& Ret)
{
    Ret.IntRet = 51;
    Ret.StringRet = "Return this string";
}

std::tuple<int, std::string>ReturnTwoTuple()
{
    return { 51, "Return this string" };
}