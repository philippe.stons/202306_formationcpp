#include "LValue_RValue.h"
#include "Debuging/ObjectWithCopyMove/ObjCpyMvCnstr.h"
#include <iostream>
#include <vector>
#include <memory>

namespace L_R_Value
{
    namespace 
    {
        template<typename T>
        inline void printFirst(T i)
        {
              std::cout << i << "\n";
        }

        template<>
        inline void printFirst<int>(int i)
        {
            std::cout << i << "\n";
        }

        template<typename First>
        inline void printPerfForward(First&& f)
        {
            printFirst<First>(f);
        }

        template<typename First, typename... Args>
        inline void printPerfForward(First&& f, Args&&... args)
        {
            printFirst<First>(f);
            if(sizeof...(Args))
                printPerfForward(std::forward<Args>(args)...);
        }

        template<typename... Args>
        inline void printMultiple(Args&&... args)
        {
            printPerfForward(std::forward<Args>(args)...);
        }

        class PerfectForwardEx
        {
        public:
            PerfectForwardEx()
            {

            }

            ~PerfectForwardEx()
            {

            }

            void SetObject(TestObject&& obj)
            {
                m_Obj = std::make_unique<TestObject>(obj);
            }

            void SetPerfectForwardObject(TestObject&& obj)
            {
                m_Obj = std::make_unique<TestObject>(std::forward<TestObject>(obj));
            }

            void PrintObjectStat()
            {
                m_Obj->PrintObjectStat();
                m_Obj->ResetObjectStat();
                std::cout << std::endl;
            }

        private:
            std::unique_ptr<TestObject> m_Obj;
        };
    }
    
    void moveSemantic()
    {
        std::vector<int> V1;
        V1.resize(9000);

        /*
        If the object contain a pointer to a very large object,
        then move will be faster than copy.

        Because copy will copy that large object, and move will
        steal the large object.

        We will see that later on with the move constructor.
        */
        std::vector<int> V2 = std::move(V1);

        /*
        This will segfault, because V1 has been moved.
        */
        std::cout << V1[5321] << std::endl;
    }

    void perfectForward()
    {
        printMultiple(55,32, 92, 61, 963, 7521, 98, 611);
        
        std::cout << std::endl;
        std::cout << std::endl;

        PerfectForwardEx perfFwd;
        perfFwd.SetObject(TestObject());
        perfFwd.PrintObjectStat();
        perfFwd.SetPerfectForwardObject(TestObject());
        perfFwd.PrintObjectStat();

    }

    void RValueReference()
    {
        // i is an LValue
        // 5 is an RValue
        int i = 5;
        int& i_Ref = i; // i and i_ref are LValue

        //int& ref2 = 5; //this won't work because 5 is an RValue
                         // so it can't be referenced.

        const int& r3 = 5; // a const type& can reference to an RValue

        int&& RValRef = 5; // we can also do RValue reference.

        /*
        one relevant use of RValue reference is
        the perfect forwarding.
        */
        std::cout << "------------------------------------------------------------------\n\n";
        std::cout << "PerfectForward: \n\n";
        perfectForward();

        std::cout << "\n------------------------------------------------------------------\n\n";

        /*
        the other one is the move semantic.
        */
        std::cout << "MoveSemantic: \n\n";
        moveSemantic();
        std::cout << "\n------------------------------------------------------------------\n\n";
    }

    int AddNbr(int i, int j)
    {
        return i + j;
    }

    void LeftAndRightValueExample()
    {
        /*
            This part is not really relevant before C++ 11
        */
        /*
        A left value is a value inside a variable.
        A right value is a temporary value.

        So an LValue is inside the heap or stack or inside .data
        an RValue is stored within the program and not in memory.
        
        */

        // i is an LValue
        // 5 is an RValue
        int i = 5;

        // both j and i are LValue
        int j = i;

        // i + 9 is an RValue
        j = i + 9;
        
        // both LValue
        int* i_ptr = new int[2];

        // i_ptr[0] is an LValue, [0] is only use to shift the address
        i_ptr[0] = i;

        // *(i_ptr + 1) is an lvalue because it is a memory address shift
        *(i_ptr + 1) = j;

        //AddNbr(i, j) is an RValue
        j = AddNbr(i, j);
        // j * i is an RValue
        i = j * i;

        RValueReference();
    }
}