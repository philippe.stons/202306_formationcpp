#include "Exception.h"
#include <exception>
#include <functional>
#include <iostream>
#include <string>

namespace Sonia {
    void initialize(std::function<void()> cb) {
        try {
            cb();
        } catch(int e) {

        }
    }
}

namespace Sonia2 {
    void initialize() {
        std::cout << "INITIALIZE" << std::endl;
    }

    void run(std::function<void()> cb) {
        try {
            cb();
        } catch(int e) {

        }
    }
}

namespace Exception
{
    class ExampleException: public std::exception
    {
        public:
            ExampleException(int status, std::string msg, int error_no)
                : status(status), msg(msg), error_no(error_no)
            {
            }

            int status;
            std::string msg;
            int error_no;
    };
    
    class AccessDeniedException: public std::exception
    {
        public:
            AccessDeniedException(std::string roleDenied, std::string ressource, std::string file)
                : roleDenied(roleDenied), ressource(ressource), file(file)
            {
                std::cout << "CONSTRUCTOR" << roleDenied << std::endl;
            }

            const char* what() const throw()
            {
                std::string msg = roleDenied + " tried to access " + ressource + " " + file;

                return msg.c_str();
            }

            std::string roleDenied;
            std::string ressource;
            std::string file;
    };

    #define AccessDeniedException(role)     AccessDeniedException(role, "AASD", __FILE__)



    void ExceptionManager() {
        std::cout << "Unmanaged exception !" << std::endl;
        // exit(-1);
    }

    int fctThrowInt()
    {
        throw 2;
    }

    int fctThrowFloat()
    {
        throw 2.0f;
    }

    int fctThrowDouble()
    {
        throw 2.0;
    }

    int fctThrowInt2(int i) // throw (int)
    {
        if(i % 2 == 0) 
        {
            throw 5; // throw l'exception
        } else {
            throw "5"; // throw std::unexcepted
        }
    }

    void ExceptionExampleMain()
    {
        // 
        std::set_terminate(&ExceptionManager);

        try 
        {
            // fctThrowInt();
            fctThrowFloat();
            // fctThrowDouble();
        } catch(float f)
        {
            std::cout << "float exception !" << std::endl;
        }

         try
         {
            fctThrowInt2(2);
         }
         catch(int i)
         {
            std::cerr << i << '\n';
         }

         try
         {
            // fctThrowInt2(3);
         }
         catch(std::string e)
         {
            std::cerr << typeid(e).name() << '\n';
         } catch(const char* e) {
            std::cerr << typeid(e).name() << '\n';
         }
         
         try
         {
            std::cout << "HELLO" << std::endl;
            throw ExampleException(500, "Access denied", 505);
         } catch(ExampleException e)
         {
            e.error_no;
            std::cout << e.msg << std::endl;
         }

         try
         {
            throw AccessDeniedException("ROLE_USER");
         }
         catch(const AccessDeniedException& e)
         {
            std::cout << e.what() << std::endl;
         }
         catch(const std::exception& e)
         {
            std::cerr << e.what() << '\n';
            std::cerr << typeid(e).name() << '\n';
         }

         try
         {
            throw AccessDeniedException("TEST");
         }
         catch(...)
         {
            std::cerr << "CATCH ALL" << '\n';
         }
    }
}

void myWorkFonction() {
    std::cout << "WORK" << std::endl;
}

// int main() {
//     Sonia::initialize([&] {
//         std::cout << "WORK" << std::endl;
//     });

//     Sonia::initialize(&myWorkFonction);

//     Sonia2::initialize();
//     Sonia2::run(&myWorkFonction);

//     Exception::ExceptionExampleMain();
// }