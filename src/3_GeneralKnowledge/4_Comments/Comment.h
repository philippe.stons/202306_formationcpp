#pragma once

/**
* @brief a brief description
* @param value an int value to pass
* @param value2 a second int value to pass.
* @return the sum of the two int 
* @post if somethig is changed after the execution of this functiom.
*/
inline int AdditionComment(int value, int value2)
{
    return value + value2;
}