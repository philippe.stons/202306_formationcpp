#include "Namespace.h"

//Anonymous namespace to declare file global variables.
namespace
{
    int GlobalValue = 5;
}

namespace N3
{

}

void NS1::NamespaceFunction()
{
    NS2::NamespaceFunction();
}

void NS2::NamespaceFunction()
{

}