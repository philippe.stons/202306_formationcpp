#include "Sort.h"
#include <vector>
#include <array>

#include <stdlib.h>     // rand
#include <algorithm>    // std::sort
#include <iostream>     // std::cout 

namespace Sorting
{
    /*
    std::begin and std::end are not available with C++ 98.
    but you can write them yourself.
    */
    template<class T, std::size_t N>
    T* begin(T (&arr)[N])
    {
        return &arr[0];
    }

    template<class T, std::size_t N>
    T* end(T (&arr)[N])
    {
        return arr + N;
    }

    /*
    Custom comparator
    */
    template<typename T>
    bool CustomCompare(const T& lhs, const T& rhs)
    {
        if(lhs > 5 && rhs > 5)
        {
            return lhs < rhs;
        }

        return false;
    }

    void SortArraysExample()
    {
        std::array<int, 20> Array;
        std::vector<int> Vector;
        int C_Array[20];
        Vector.reserve(20);

        for(int i = 0; i < 20; i++)
        {
            Array[i] = rand() % 20;
            Vector.emplace_back(Array[i]);
            C_Array[i] = Array[i];
        }

        std::sort(Array.begin(), Array.end());
        std::sort(Vector.begin(), Vector.end());
        //Won't in C++ 98
        //std::sort(std::begin(C_Array), std::end(C_Array));
        //Can be done manually: 
        std::sort(begin(C_Array), end(C_Array));

        std::cout << "\nDefault sort\n\n";

        for(int i = 0; i < 20; i++)
        {
            std::cout << Array[i] << std::endl;
            std::cout << Vector[i] << std::endl;
            std::cout << C_Array[i] << std::endl;
        }

        std::cout << "\nCustom sort\n\n";

        std::sort(Vector.begin(), Vector.end(), CustomCompare<int>);

        for(int i = 0; i < Vector.size(); i++)
        {
            std::cout << Vector[i] << std::endl;
        }
        
        std::cout << "\nReverse sort\n\n";
        /*
        use rbegin and rend
        */
        std::sort(Vector.rbegin(), Vector.rend());

        for(int i = 0; i < Vector.size(); i++)
        {
            std::cout << Vector[i] << std::endl;
        }
    }
}