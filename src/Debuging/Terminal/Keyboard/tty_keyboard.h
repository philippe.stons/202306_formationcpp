#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

int tty_start_keyboard();

int tty_stop_keyboard();

short tty_get_key_state(unsigned short key);

#ifdef __cplusplus
}
#endif