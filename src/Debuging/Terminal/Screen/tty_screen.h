#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

void tty_clear_screen();

#ifdef __cplusplus
}
#endif