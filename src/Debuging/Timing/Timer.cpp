#include "Timer.h"

Timer::Timer()
    : name("")
{
    start = std::chrono::high_resolution_clock::now();
}

Timer::Timer(const std::string& name)
    : name(name + " ")
{
    start = std::chrono::high_resolution_clock::now();
}

Timer::~Timer()
{
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    double ms = elapsed.count() * 1000.0;
    std::cout << "Fonction " << name <<  "took : " << ms << "ms to execute." << std::endl;
}