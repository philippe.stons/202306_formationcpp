#include "Cast.h"

#include <iostream>

void C_CastExemple()
{
    int OriginalVar = 56;

    float CastVar = (float)OriginalVar;
    std::cout << "\nCast int To float : \n"
        << "Original Var value is: " << OriginalVar << "\n"
        << "Casted Var value is: " << CastVar << "\n";

    float* PtrCastVar = (float*)&OriginalVar;
    std::cout << "\nCast int To float* : \n"
        << "Original Var value is: " << OriginalVar << "\n"
        << "Casted Var value is: " << *PtrCastVar << "\n";

    //Do not forget the "&" before the variable you want to cast.
    //Else it won't return a pointer.
    /*
    float* PtrCastVar = (float*)OriginalVar;
    std::cout << "\nCast int To float* : \n"
        << "Original Var value is: " << OriginalVar << "\n"
        << "Casted Var value is: " << *PtrCastVar << "\n";
    */
}

void Cpp_CastExemple()
{
    int OriginalVar = 56;

    float StaticCastVar = static_cast<float>(OriginalVar);

    std::cout << "\nstatic_cast<>() : \n"
        << "Original Var value is: " << OriginalVar << "\n"
        << "Casted Var value is: " << StaticCastVar << "\n";

    float* ReinterpretCastVar = reinterpret_cast<float*>(&OriginalVar);

    std::cout << "\nreinterpret_cast<>() : \n"
        << "Original Var value is: " << OriginalVar << "\n"
        << "Casted Var value is: " << *ReinterpretCastVar << "\n";
}