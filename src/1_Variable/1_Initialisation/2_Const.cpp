#include "Const.h"

void ConstVar()
{
    const int ConstInteger = 55;

    //The value is constant so the following line won't compile
    //ConstInteger = 23;
}