#include "Initialization.h"
#include <iostream>

typedef unsigned char Byte;

struct IPv4
{
    Byte _1;
    Byte _2;
    Byte _3;
    Byte _4;
};

union WeirdUnion
{
    int int_val;
    float float_val;
};

void initializationExample()
{
    int i = 5;
    float f = 2.6f;

    char c = 'C';
    std::cout << c << std::endl;
    std::cout << (int)c << std::endl;

    c += 1;

    std::cout << c << std::endl;
    std::cout << (int)c << std::endl;

    unsigned int uint = 55;
    std::cout << uint << std::endl;

    uint -= 56;
    std::cout << uint << std::endl;

    //Ternary operators:
    // condition ? if true : if false ;
    bool b = i == 5 ? true : false;
    i != 3 ? std::cout << "true\n" : std::cout << "false\n";

    if(i != 12 || i != 9)
    {

    }

    if(0b0100 & 0b0110) // 0b0100  
    {

    }
}