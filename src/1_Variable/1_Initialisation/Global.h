#pragma once

//Declare a global variable in a header file.
extern int GlobVar;

//The only global variables that are usually accepted are const global
//Before C++11 & 17
//extern const int GlobalConstVar;
//With C++11
//constexpr int GlobalConstVar{ 10 };
//Or
//constexpr int GlobalConstVar = 10;
//With C++17
inline extern const int GlobalConstVar = 10;
//Or with define
#define D_GlobalConstVar 10

void PrintGlobalVar();

void PrintSourceFileGlobalVar();