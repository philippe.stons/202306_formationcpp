#include "SmartPointer.h"

//REQUIRED!
#include <memory>
#include <iostream>

void SmartPointerExample()
{
    std::unique_ptr<int> UniquePtr;

    UniquePtr = std::make_unique<int>(5);

    //Un comment to see the error
    //unique_ptr can't be duplicate.
    //std::unique_ptr<int>UniquePtr2 = UniquePtr;
    //std::unique_ptr<int>UniquePtr2 = std::make_unique<int>(UniquePtr);
    std::unique_ptr<int>UniquePtr2 = std::make_unique<int>(*UniquePtr);

    std::cout << "UniquePtr Address: " << &(*UniquePtr) << std::endl;
    std::cout << "UniquePtr value: " << *UniquePtr << std::endl;
    //Here we can see that UniquePtr2 does not point to the same address as UniquePtr.
    std::cout << "UniquePtr2 Address: " << &(*UniquePtr2) << std::endl;
    std::cout << "UniquePtr2 value: " << *UniquePtr2 << std::endl;

    //THIS IS A PRETTY BAD UNSAGE OF SMART POINTER!!!!
    //int* Ptr = &(*UniquePtr);

    std::shared_ptr<int> SharedPtr;

    SharedPtr = std::make_shared<int>(7);

    //std::shared_ptr<int> SharedPtr2(SharedPtr);
    std::shared_ptr<int> SharedPtr2 = SharedPtr;

    std::weak_ptr<int> WeakPtr = SharedPtr;
    std::weak_ptr<int> WeakPtr2 = SharedPtr;

    if(std::shared_ptr<int> SharedPtr3 = WeakPtr2.lock())
    {
        std::cout << "We can now access SharedPtr value: " << *SharedPtr3 << std::endl;
    }

    std::shared_ptr<int> SharedPtr3 = WeakPtr2.lock();
    if(SharedPtr3)
    {
        std::cout << "We can now access SharedPtr value: " << *SharedPtr3 << std::endl;
    }

    *SharedPtr2 = 12;

    std::cout << *SharedPtr2 << std::endl;
}

struct Object
{
    Object()
    {
        val = 0;
        std::cout << "OBJECT CREATED" << std::endl;
    }

    ~Object()
    {
        std::cout << "OBJECT DESTROY" << std::endl;
    }

    int val;
};

void SmartPointerLifeCycle()
{
    //UNIQUE_PTR
    std::cout << "\nUNIQUE_PTR\n";
    {
        std::unique_ptr<Object> UniquePtrLifeCycle = std::make_unique<Object>();
    }

    //SHARED_PTR
    std::cout << "\nSHARED_PTR\n";
    {
        std::shared_ptr<Object> OutsideScopeShrdPtr;
        {
            std::shared_ptr<Object> SharedPtr = std::make_shared<Object>();

            OutsideScopeShrdPtr = SharedPtr;

            SharedPtr->val = 5;
        }

        std::cout << "Obj value " << OutsideScopeShrdPtr->val << std::endl;
    }

    //WEAK_PTR
    std::cout << "\nWEAK_PTR\n";
    {
        std::weak_ptr<Object> OutsideScopeWeakPtr;
        {
            std::shared_ptr<Object> SharedPtr = std::make_shared<Object>();

            OutsideScopeWeakPtr = SharedPtr;

            SharedPtr->val = 5;
        }
        if(std::shared_ptr<Object> Ptr = OutsideScopeWeakPtr.lock())
        {
            std::cout << "We can access Obj value: " << Ptr->val << std::endl;
        } else 
        {
            std::cout << "We can't access Obj value because the value is nullptr." << std::endl;
        }
    }
}