#include "Deque.h"
#include <deque>
#include <iostream>

void DequeExample()
{
    std::deque<int> DoubleEndedQueue;
    DoubleEndedQueue.push_back(55);
    DoubleEndedQueue.push_front(32);
    DoubleEndedQueue.emplace_front(97);
    DoubleEndedQueue.emplace_back(77);

    for(int element : DoubleEndedQueue)
    {
        std::cout << "Element:\t" << element << std::endl;
    }

    std::cout << "Element at 2:\t" << DoubleEndedQueue[2] << std::endl;
    std::cout << "Front element:\t" << DoubleEndedQueue.front() << std::endl;
    std::cout << "Back element:\t" << DoubleEndedQueue.back() << std::endl;

    DoubleEndedQueue.pop_front();
    DoubleEndedQueue.pop_back();

    for(int element : DoubleEndedQueue)
    {
        std::cout << "Element:\t" << element << std::endl;
    }
}