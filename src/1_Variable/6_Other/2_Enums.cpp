#include "Enums.h"
#include <iostream>

/*
This will make the compiler complains because there are multiple definition of the same
enums name. 
enum EnumExample2
{
    ENUM_EX_ARG_1,
    ENUM_EX_ARG_2,
    ENUM_EX_ARG_3,
    ENUM_EX_ARG_4
};
*/

typedef unsigned char Byte;

void EnumExample()
{
    enum EnumExample
    {
        ENUM_EX_ARG_1 = 0,
        ENUM_EX_ARG_2,
        ENUM_EX_ARG_3,
        ENUM_EX_ARG_4
    } Enum;
    
    Enum = EnumExample::ENUM_EX_ARG_1;

    int ImplicitCastToInt = Enum;

    enum class EnumClassExample
    {
        ENUM_EX_ARG_1,
        ENUM_EX_ARG_2,
        ENUM_EX_ARG_3,
        ENUM_EX_ARG_4
    };

    EnumClassExample State = EnumClassExample::ENUM_EX_ARG_1;

    if(State == EnumClassExample::ENUM_EX_ARG_1)
    {
        State = EnumClassExample::ENUM_EX_ARG_2;
    }

    enum EnumFlags
    {
        NONE    = 0x0,
        FLAG_1  = 0x1,
        FLAG_2  = 0x2,
        FLAG_3  = 0x4,
        FLAG_4  = 0x8
    };

    Byte Flags;
    Flags = EnumFlags::FLAG_1;
    Flags |= EnumFlags::FLAG_2 | EnumFlags::FLAG_4;

    //flags = 0b1011 so 8 + 2 + 1 = 11
    std::cout << (int)Flags << std::endl;
}