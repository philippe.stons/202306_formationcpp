#include "Bitfield.h"
#include <iostream>
#include <bitset>

typedef unsigned char Byte;
union Bits
{
    Byte Value;
    struct
    {
        Byte _1 : 1; // assign 1 bit to _1
        Byte _2 : 1;
        Byte _3 : 1;
        Byte _4 : 1;
        Byte _5 : 1;
        Byte _6 : 1;
        Byte _7 : 2; // assign two bit to _7
    };
};

void BitField()
{
    Bits bits;

    bits._1 = 1;
    bits._5 = 1;
    bits._7 = 1;

    std::cout << (unsigned int)bits.Value << std::endl;

    std::bitset<6> Values;
    Values[0] = 1;
    Values[4] = 1;
    Values[6] = 1;

    std::cout << Values << std::endl;
    std::cout << Values.to_ulong() << std::endl;

    std::cout << sizeof(bits) << std::endl;
    std::cout << sizeof(Values) << std::endl;

    int i = 0x523;
    int b = 0b0011;
    // int c = 0367; // 
    int d = 123;
}

enum Test{
    FLAG_1 = 0b0001,
    FLAG_2 = 0b0010,
    FLAG_3 = 0b0100,
    FLAG_4 = 0b1000,
};

enum Test2 {
    TEST_1,
};

struct { // 3 Byte
    bool flag1;
    bool flag2;
    bool flag3;
} str1;

struct { // 1 Byte
    unsigned char flag1: 1; 
    unsigned char flag2: 1; 
    unsigned char flag3: 1; 
} str2;