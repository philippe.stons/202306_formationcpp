#include "Set.h"

#include <set>
#include <iostream>

void SetExample()
{
    std::set<int, std::greater_equal<int>> Set;
    Set.insert(33);
    Set.insert(45);
    Set.insert(21);
    Set.insert(45);
    Set.insert(15);

    std::cout << "\nSet with std::greater_equal<>: \n";
    for(int elem: Set)
    {
        std::cout << "Element:\t" << elem << std::endl;
    }

    std::set<int, std::greater<int>> Set2;
    Set2.insert(33);
    Set2.insert(45);
    Set2.insert(21);
    Set2.insert(45);
    Set2.insert(15);

    std::cout << "\nSet with std::greater<>: \n";
    for(int elem: Set2)
    {
        std::cout << "Element:\t" << elem << std::endl;
    }

    std::set<int, std::less<int>> Set3;
    Set3.insert(33);
    Set3.insert(45);
    Set3.insert(21);
    Set3.insert(45);
    Set3.insert(15);

    std::cout << "\nSet with std::less<>: \n";
    for(int elem: Set3)
    {
        std::cout << "Element:\t" << elem << std::endl;
    }

    std::set<int> Set4;
    Set4.insert(33);
    Set4.insert(45);
    Set4.insert(21);
    Set4.insert(45);
    Set4.insert(15);

    std::cout << "\nSet default: \n";
    for(int elem: Set4)
    {
        std::cout << "Element:\t" << elem << std::endl;
    }

    Set4.erase(45);
    std::cout << "\n.erase(45): \n";
    for(int elem: Set4)
    {
        std::cout << "Element:\t" << elem << std::endl;
    }

    std::cout << "\nlower and upper bound: \n";
    //return the closest element that is greater or equal than 20
    std::cout << "Lower bound: " << *Set4.lower_bound(20) << std::endl;
    //return the closest element that is greater than 30
    std::cout << "Upper bound: " << *Set4.upper_bound(30) << std::endl;
}