#include "List.h"

#include <list>
#include <iostream>

void ListExample()
{
    std::list<int> IntList;

    IntList.push_back(0);
    IntList.emplace_back(5);
    IntList.push_front(8);
    IntList.emplace_front(3);

    for(int elem : IntList)
    {
        std::cout << "Element:\t" << elem << std::endl;
    }

    /*
    List are not contiguous blocks of memory.
    So they can't be access using [].
    */
    //std::cout << IntList[20] << std::endl;
}