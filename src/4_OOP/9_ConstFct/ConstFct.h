#pragma once

#include <memory>

namespace ConstFct
{
    class Object
    {
    public:
        Object();
        ~Object();

        /*
        The first const say that it will return a const int.
        the second const means that we won't be able to change anything
        inside the object in this function.
        */
        inline const int GetValue() const
        {
            //we can't change anything inside the object due to the second const
            //value = 52;

            return value;
        }

        inline int NonConstGetValue() {
            return value;
        }

        // inline const Object GetObj() const 
        // {
        //     return *this;
        // }

        // inline const Object* GetObjPtr() const 
        // {
        //     return this;
        // }

        // inline std::shared_ptr<const Object> GetObjShrPtr() const 
        // {
        //     return std::make_shared<const Object>(this);
        // }

    private:
        int value;
    };

    void ConstFctExample();
}