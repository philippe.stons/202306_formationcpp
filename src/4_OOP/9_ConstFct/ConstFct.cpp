#include "ConstFct.h"
#include <iostream>

namespace ConstFct
{
    Object::Object()
        : value(25)
    {
        
    }

    Object::~Object()
    {
        
    }

    void ConstFctExample()
    {
        const int test =55;
        int test2 = test;
        Object O;
        int i = O.GetValue();
        // Object O2 = O.GetObj();
        // int c = O.GetObj().NonConstGetValue();
        // Object* O3 = O.GetObjPtr();
        // const Object* O3 = O.GetObjPtr();
        // std::shared_ptr<const Object> O4 = O.GetObjShrPtr();

        std::cout << i << std::endl;
    }
}