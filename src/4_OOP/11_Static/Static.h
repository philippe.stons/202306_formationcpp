/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
#pragma once

namespace StaticInObject
{
    void StaticExample();

    class StaticObject
    {
    public:
        StaticObject() = delete;
        StaticObject(int i);
        ~StaticObject();

        static void StaticFunction();
        static void StaticFunction2(StaticObject* _this);

    private:
        //STATIC "MEMBERS" SHOULD NEARLY ALWAYS BE PRIVATE!!
        static int s_Val;

        int m_Val;

    };
}