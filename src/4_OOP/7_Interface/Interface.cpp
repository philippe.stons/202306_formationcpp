/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
#include "Interface.h"
#include <iostream>

namespace Interface
{
    AbstractClass::AbstractClass() {
        
    }

    AbstractClass::~AbstractClass() {

    }

    void AbstractClass::AbstractFunc()
    {

    }

    Derived::Derived()
    {

    }

    Derived::~Derived()
    {

    }

    void Derived::InterfaceFunc()
    {
        
    }

    void Derived::InterfacePrintValue()
    {
        std::cout << "Interface define var: " << m_InterfaceVar << 
            "\nAbstract class defined var: " << m_AbstractVar << std::endl;
    }

    void Derived::AbstractPureVirtual()
    {

    }
}
