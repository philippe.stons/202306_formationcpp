//
// Created by rmdir on 6/26/23.
//

#include "MultipleInheritance.h"
#include <iostream>

void MultipleInheritanceBase::test() {
    std::cout << "Hello" << std::endl;
}

void MultipleInheritance::multipleInheritanceExample() {
    Derived3 test = Derived3();

    // Ambiguous variable !!!
//    test.m_Variable = 1;
//    std::cout << test.m_Variable << std::endl;
    test.Derived1::m_Variable = 2;
    std::cout << test.Derived1::m_Variable << std::endl;
    test.Derived2::m_Variable = 3;
    std::cout << test.Derived2::m_Variable << std::endl;

    // test.test();
    test.Derived1::test();

    derived3 test_c = derived3();

    test_c.a = 1;
    std::cout << test_c.a << std::endl;
    test_c.derived1.a = 2;
    std::cout << test_c.a << std::endl;
    test_c.derived2.a = 3;
    std::cout << test_c.a2 << std::endl;
}
