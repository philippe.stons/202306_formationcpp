//
// Created by rmdir on 6/26/23.
//

#ifndef FORMATION_C_CPP_MULTIPLEINHERITANCE_H
#define FORMATION_C_CPP_MULTIPLEINHERITANCE_H

class MultipleInheritanceBase {
public:
    int m_Variable;

    void test();
};

class Derived1 : public MultipleInheritanceBase {
};

class Derived2 : public MultipleInheritanceBase {
};

class Derived3 : public Derived1, public Derived2 {
};

typedef struct _base
{
    // index in the file table
    int a;
} __attribute__((packed)) base;

typedef struct _derived1
{
    // index in the file table
    union {
        _base base;
        int a;
    };
} __attribute__((packed)) derived1;

typedef struct _derived2
{
    // index in the file table
    union {
        _base base;
        int a;
    };
} __attribute__((packed)) derived2;

typedef struct _derived3
{
    // index in the file table
    union {
        _derived1 derived1;
        int a;
    };

    union {
        _derived2 derived2;
        int a2;
    };
} __attribute__((packed)) derived3;

class MultipleInheritance {
public:
    static void multipleInheritanceExample();
};


#endif //FORMATION_C_CPP_MULTIPLEINHERITANCE_H
