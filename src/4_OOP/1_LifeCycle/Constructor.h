#pragma once

namespace Constructor
{
    void ConstructorExample();
    
    struct Object
    {
        //Default constructor
        Object();

        //Automatically assigned except if deleted
        //Object() = delete;

        Object(int arg_1, float arg_2);

        //the previous constructor were implicit constructor
        explicit Object(float arg_1);
        //Object(float arg_1);

        int Value_1;
        float Value_2;
    };
}