/*
 *   Copyright (c) 2023 
 *   All rights reserved.
 */
#pragma once

#include <vector>

namespace Visibility
{
    struct StructObject
    {
        int m_Int;
    };

    class ClassObject
    {
        int m_Int;
    };

    class Object
    {
    public:
        Object();
        Object(int Int1, int Int2);
        ~Object();

        void ObjFunction();
        
    private:
        int Add();

        int m_Int, m_Int2;
        std::vector<int> vec;
    };

    void VisibilityExample();
}