#include "OptionalReturn.h"
#include <optional>
#include <fstream>
#include <iostream>

std::optional<std::string> OptionalReturn(const char* filePath);

void OptionalReturnExample()
{
    std::optional<std::string> datas = OptionalReturn("data.txt");
    if(datas)
    {
        std::cout << "Success!\n";
        std::string str = *datas;
    } else 
    {
        std::cout << "Failed to open file data.txt.\n";
    }

    std::optional<std::string> datas2 = OptionalReturn("data.txt");
    std::string str = datas2.value_or("something else");
    std::cout << str << std::endl;
}

std::optional<std::string> OptionalReturn(const char* filePath)
{
    std::ifstream stream(filePath);
    if(stream)
    {
        std::string result = filePath;
        // DO SOMETHING
        stream.close();
        return result;
    }
    //return empty optional
    return {};
}