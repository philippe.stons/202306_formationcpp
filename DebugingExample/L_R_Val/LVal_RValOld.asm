	.file	"LVal_RVal.cpp"
	.text
	.globl	GlobalI
	.data
	.align 4
	.type	GlobalI, @object
	.size	GlobalI, 4
GlobalI:
	.long	3
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	endbr64
	pushq	%rbp				; save return address
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp			; base stack pointer = top stack pointer
	.cfi_def_cfa_register 6
	subq	$64, %rsp			; resize the stack
	movl	%edi, -52(%rbp)		; first arg (argc)
	movq	%rsi, -64(%rbp)		; second arg (argv) !! this is not _start
								; so argc and argv are not into the stack.
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax			; clear eax
	
	movl	$3, -44(%rbp)		; int i = 3;
	movl	$12, -44(%rbp)		; i = 12;
	leaq	-44(%rbp), %rax		; int& i_Ref = i;
	movq	%rax, -32(%rbp)		; int& i_Ref = i;
	movl	$9, GlobalI(%rip)	; GlobalI = 9;
	movl	$5, %eax			; = 5 put it into eax
	movl	%eax, -40(%rbp)		; = 5 put it into the stack
	leaq	-40(%rbp), %rax		; const int& r3 = 5; the ref part
	movq	%rax, -24(%rbp)		; const int& r3 = 5; the ref part
	movl	$7, %eax			; = 7 put it eax
	movl	%eax, -36(%rbp)		; = 7 put it into the stack
	leaq	-36(%rbp), %rax		; int&& RValRef = 7; the ref part
	movq	%rax, -16(%rbp)		; int&& RValRef = 7; the ref part
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	je	.L3
	call	__stack_chk_fail@PLT
.L3:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 9.2.1-9ubuntu2) 9.2.1 20191008"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
