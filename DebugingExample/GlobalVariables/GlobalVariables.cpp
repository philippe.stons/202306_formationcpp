#include <iostream>

#ifdef USING_DEFINE
#define GlobalVar 12
#else
inline extern const int GlobalVar = 12;
//const int GlobalVar = 12;
#endif

int main(int argc, char** argv)
{
    std::cout << GlobalVar << std::endl;

    return 0;
}