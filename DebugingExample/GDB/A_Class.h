#pragma once

#include <string>
#include <unordered_map>

class GDB_ExampleClass
{
public:
    GDB_ExampleClass();
    ~GDB_ExampleClass();

    void ManipulateData();
    void SetData(const char* name, int value);
    int GetData(const char* name);
    void PrintData(const char* name);
    void AddToData(const char* name, int value);

private:
    bool SearchData(const char* name);

    std::unordered_map<std::string, int> m_Data;
};